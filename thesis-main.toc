\contentsline {chapter}{DANH S\IeC {\'A}CH B\IeC {\h A}NG}{ii}{chapter*.2}
\contentsline {chapter}{DANH S\IeC {\'A}CH H\IeC {\`I}NH V\IeC {\~E}}{iii}{chapter*.3}
\contentsline {chapter}{\numberline {1}T\IeC {\h \ocircumflex }ng quan v\IeC {\`\ecircumflex } ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n x\IeC {\^a}m nh\IeC {\d \acircumflex }p m\IeC {\d a}ng}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}C\IeC {\'a}c v\IeC {\'\acircumflex }n \IeC {\dj }\IeC {\`\ecircumflex } v\IeC {\`\ecircumflex } an to\IeC {\`a}n th\IeC {\^o}ng tin}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}M\IeC {\d u}c ti\IeC {\^e}u c\IeC {\h u}a vi\IeC {\d \ecircumflex }c \IeC {\dj }\IeC {\h a}m b\IeC {\h a}o an to\IeC {\`a}n th\IeC {\^o}ng tin}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}T\IeC {\'\acircumflex }n c\IeC {\^o}ng m\IeC {\d a}ng}{2}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n x\IeC {\^a}m nh\IeC {\d \acircumflex }p m\IeC {\d a}ng}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}S\IeC {\d \uhorn } c\IeC {\`\acircumflex }n thi\IeC {\'\ecircumflex }t c\IeC {\h u}a ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n x\IeC {\^a}m nh\IeC {\d \acircumflex }p m\IeC {\d a}ng}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Ph\IeC {\^a}n lo\IeC {\d a}i ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n x\IeC {\^a}m nh\IeC {\d \acircumflex }p}{4}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Ph\IeC {\^a}n lo\IeC {\d a}i theo k\IeC {\~y} thu\IeC {\d \acircumflex }t ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n}{4}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Ph\IeC {\^a}n lo\IeC {\d a}i theo c\IeC {\^o}ng ngh\IeC {\d \ecircumflex }}{5}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}K\IeC {\'\ecircumflex }t lu\IeC {\d \acircumflex }n}{7}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}M\IeC {\d u}c ti\IeC {\^e}u c\IeC {\h u}a \IeC {\dj }\IeC {\`\ocircumflex } \IeC {\'a}n}{7}{section.1.5}
\contentsline {chapter}{\numberline {2}M\IeC {\^o} h\IeC {\`\i }nh Boosted Tree v\IeC {\`a}o ph\IeC {\'a}t hi\IeC {\d \ecircumflex }n x\IeC {\^a}m nh\IeC {\d \acircumflex }p m\IeC {\d a}ng}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u b\IeC {\d \ocircumflex } d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u UNSW-NB15}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p thu th\IeC {\d \acircumflex }p d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}C\IeC {\'\acircumflex }u h\IeC {\`\i }nh cho IXIA}{9}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Danh s\IeC {\'a}ch c\IeC {\'a}c \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng trong b\IeC {\d \ocircumflex } d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{10}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a}}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Th\IeC {\d \uhorn }c nghi\IeC {\d \ecircumflex }m}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}H\IeC {\d \ecircumflex } th\IeC {\'\ocircumflex }ng m\IeC {\'a}y t\IeC {\'\i }nh}{13}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}C\IeC {\'a}c ch\IeC {\uhorn }\IeC {\ohorn }ng tr\IeC {\`\i }nh v\IeC {\`a} th\IeC {\uhorn } vi\IeC {\d \ecircumflex }n ph\IeC {\`\acircumflex }n m\IeC {\`\ecircumflex }m}{13}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}Scikit-learn}{13}{subsubsection.2.3.2.1}
\contentsline {subsection}{\numberline {2.3.3}C\IeC {\`a}i \IeC {\dj }\IeC {\d \abreve }t}{14}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Ti\IeC {\`\ecircumflex }n x\IeC {\h \uhorn } l\IeC {\'y} d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{14}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Lo\IeC {\d a}i b\IeC {\h o} c\IeC {\'a}c \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng d\IeC {\uhorn } th\IeC {\`\uhorn }a}{14}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}X\IeC {\h \uhorn } l\IeC {\'y} c\IeC {\'a}c d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u d\IeC {\d a}ng nominal}{14}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}C\IeC {\'a}c m\IeC {\^o} h\IeC {\`\i }nh th\IeC {\d \uhorn }c nghi\IeC {\d \ecircumflex }m}{15}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Ph\IeC {\^a}n l\IeC {\'\ohorn }p 2 nh\IeC {\~a}n}{15}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Ph\IeC {\^a}n l\IeC {\'\ohorn }p 10 nh\IeC {\~a}n}{16}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Nh\IeC {\d \acircumflex }n x\IeC {\'e}t, \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a}}{18}{section.2.6}
\contentsline {chapter}{\numberline {3}T\IeC {\h \ocircumflex }ng k\IeC {\'\ecircumflex }t}{19}{chapter.3}
\contentsline {chapter}{T\IeC {\`A}I LI\IeC {\d \ECIRCUMFLEX }U THAM KH\IeC {\h A}O}{20}{chapter*.38}
